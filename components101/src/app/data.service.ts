import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  constructor() { }

  names = ['rashid', 'ahmed', 'ali']

  myData() {
    return 'This is my test data.'
  }
}
