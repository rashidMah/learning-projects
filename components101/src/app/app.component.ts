import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service'
import {trigger, state, style, transition, animate, keyframes} from '@angular/animations'

@Component({
  selector: 'app-root',
  // templateUrl: './app.component.html',
  template: `<h1>Test Head</h1>
             <div *ngIf='myArr; then templ1 else templ2'>Rashid</div>

             <ng-template #templ1>I'm working here</ng-template>
             <ng-template #templ2>You're working here</ng-template>

             <img src = '{{angularLogo}}'>
             <img [src] = 'angularLogo'>
             <img bind-src = 'angularLogo'>

            //  Property Binding (Componenet to Template(View))
             <button [disabled] = "buttonStatus"> My button</button> 

            //  Event Binding (View to Component) -- Listen to user action
             <button (click)="myEvent($event)">Click Me</button>

             <h1 [class.red-title]="titleClass">Hello!</h1>
             <h1 [ngClass] = "titleClasses">My Brother</h1>

             <h1 [style.color]="titleStyle ? 'green' : 'blue'">Yes I'm here</h1>
             <h1 [ngStyle] = "titleStyles">What are you doing?</h1>

             <h1>{{someProperty}}</h1>

             <p [@myAwesomeAnimation]='state' (click)='animateMe()'>
                I'll animate
             </p>
  `,
  // styleUrls: ['./app.component.css']
  styles: [`
          h1 {
            text-decoration: underline;
          }

          .red-title {
            color:red;
          }
          .large-title {
            font-size: 4em;
          }

          p {
            width: 200px;
            background: lightgray;
            margin: 100px auto;
            text-align: center;
            padding: 20px;
            font-size: 1.5em;
          }
  `],
  animations: [
    trigger('myAwesomeAnimation', [

      state('small', style({
        transform: 'scale(1)',
      })),
      state('large', style({
        transform: 'scale(1.5)',
      })),

      // transition('small <=> large', animate('500ms ease-in', style({
      //   transform: 'translateY(40px)'
      // }))),

      transition('small <=> large', animate('500ms ease-in', keyframes([
        style({
          opacity: 0, transform: 'translateY(-75%)', offset: 0
        }),
        style({
          opacity: 1, transform: 'translateY(35%)', offset: .5
        }),
        style({
          opacity: 1, transform: 'translateY(0)', offset: 1
        }),
      ]))),
    ]),

  ]
})
export class AppComponent implements OnInit{
  title = 'app';
  myArr = false;
  buttonStatus = TextTrackCue; 
  titleClass = false
  titleClasses = {
    'red-title': false,
    'large-title': true
  }

  titleStyle = false;
  titleStyles = {
    'color': 'orange',
    'font-size': '4em'
  }

  angularLogo = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSuO1hxSynUCum0-m4YCjPE037fsNAmh2DMBHu-gMxkV3FFikqi1A'

  someProperty:string = '';

  state: string = 'small';

  constructor(private dataService:DataService) {
  }

  ngOnInit() {
    console.log(this.dataService.names)
    this.someProperty = this.dataService.myData()
  }

  myEvent(event) {
    console.log(event)
  }

  animateMe() {
    this.state = (this.state === 'small' ? 'large' : 'small')
  }
}
