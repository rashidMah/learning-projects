import { TestBed, inject } from '@angular/core/testing';

import { DaraService } from './dara.service';

describe('DaraService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DaraService]
    });
  });

  it('should be created', inject([DaraService], (service: DaraService) => {
    expect(service).toBeTruthy();
  }));
});
